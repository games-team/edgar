Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: edgar
Upstream-Contact: info@parallelrealities.co.uk
Source: <http://www.parallelrealities.co.uk/games/edgar/>

Files: *
Copyright: (C) 2009-2024 Parallel Realities
License: GPL-2.0+

Files: icons/edgar.appdata.xml
Copyright: 2014 Edgar Muniz Berlinck <edgar.vv@gmail.com>
License: CC0-1.0

# doc/music_and_sound_licences
# Music from Open Game Art
# http://opengameart.org

Files:
 music/Decision.ogg
 music/Heroes?Theme_0.ogg
 music/Heroic?Minority.ogg
 music/Medicine.ogg
 music/Menu_loop.ogg
 music/Mystical?Theme.ogg
 music/So??let?see??what?you?can_0.ogg
 music/Steeps?of?Destiny.ogg
 music/Without.ogg
Copyright: Alexandr Zhelanov
License: CC-BY-3.0

Files:
 music/A?wintertale_0.ogg
 music/Battle?in?the?winter.ogg
Copyright: Johan Brodd
License: CC-BY-3.0

Files: music/A?tricky?puzzle.ogg
Copyright: Pant0don
License: CC-BY-SA-3.0

Files: music/Avgvst?-?Mushrooms_1.ogg
Copyright: avgvst
License: CC-BY-3.0

Files: music/Battle.ogg
Copyright: Zehydra
License: CC-BY-3.0

Files: music/beauty_of_chaos.ogg
Copyright: grindhold and grindhold.de
License: CC-BY-SA-3.0

Files: music/Danza?del?bosque.ogg
Copyright: xXUnderTowerXx
License: CC-BY-SA-3.0

Files: music/Lazy?Day?v0_9.ogg
Copyright: FoxSynergy
License: CC-BY-3.0

Files: music/MysticalCaverns.ogg
Copyright: MichaelTheCrow
License: CC-BY-3.0

Files: music/ratsrats_0.ogg
Comment: Rat Sewer
Copyright: HaelDB
License: CC-BY-3.0

Files: music/Red?Curtain.ogg
Copyright: HorrorPen
License: CC-BY-3.0

Files: music/Ruinous?Laments(5.6).ogg
Copyright: Eliot Corley
License: CC-BY-SA-3.0

Files: music/Szymon?Matuszewski?-?Hope.ogg
Copyright: Szymon Matuszewski
License: CC-BY-3.0

Files: music/Undead?Rising.ogg
Copyright: Matthew Pablo
License: CC-BY-SA-3.0

Files: music/Zander?Noriega?-?Fight?Them?Until?We?Cant.ogg
Copyright: Zander Noriega
License: CC-BY-3.0


Files: debian/*
Copyright: 2011-2012 Joe Nahmias <jello@debian.org>, 
 2022 Matt Barry <matt@hazelmollusk.org>,
 2023-2024 Alexandre Detiste <tchet@debian.org>,
License: GPL-2.0+

License: CC0-1.0
 On Debian systems, the complete text of the CC0 1.0 Universal
 can be found in "/usr/share/common-licenses/CC0-1.0".

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
